package ccd.attendancemanager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.ScrollView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private CalendarView calendarView;
    private RecyclerView rv;
    private ItemTouchHelper itemTouchHelper;
    private DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        calendarView = (CalendarView) findViewById(R.id.calender_view);
        calendarView.setVisibility(View.GONE);

        DateCalendar currentCal = new DateCalendar();
        String date = new SimpleDateFormat("dd MMMM", Locale.US).format(currentCal.getTime());

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        List<String> subjectsNoRep = new JsonParser(this).getSubjectsFromJson();
        List<String> subjectsRep = new JsonParser(this).getSubjectsWithRepetitionFromJson();
        dbHelper = new DatabaseHelper(this, subjectsNoRep);

        if (!sp.contains("id")) {
            setupSP(sp, subjectsRep);
        } else {
            if (sp.getInt("id", 0) != currentCal.getId()) {
                dbHelper.addStat(new DayRecord(sp, subjectsRep));
                setupSP(sp, subjectsRep);
            }
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(date);
        }

        calendarView.setOnDateChangeListener(dateChangeListener());

        setRecycleView();

    }

    private void setupSP(SharedPreferences sp, List<String> list) {
        sp.edit().putInt("id", new DateCalendar().getId()).apply();
        for (String key : list) {
            key = key.replaceAll("-", "_");
            sp.edit().putInt(key + "_total", 0).apply();
            sp.edit().putInt(key + "_attended", 0).apply();
        }
    }

    private void initSwipe(PeriodAdapter adapter, boolean firstTime) {
        if (!firstTime) detachItemTouchHelper(itemTouchHelper);
        itemTouchHelper = new ItemTouchHelper(new SwipeHelper(this, adapter));
        itemTouchHelper.attachToRecyclerView(rv);
    }

    private void detachItemTouchHelper(ItemTouchHelper itemTouchHelper) {
        itemTouchHelper.attachToRecyclerView(null);
    }

    private void setRecycleView() {
        rv = (RecyclerView) findViewById(R.id.recycler_view);
        rv.setLayoutManager(new LinearLayoutManager(this));
        PeriodAdapter rvAdapter = new PeriodAdapter(this, new DateCalendar());
        rv.addItemDecoration(new SpacingDecoration(10));
        rv.setAdapter(rvAdapter);
        initSwipe(rvAdapter, true);
        registerForContextMenu(rv);
    }

    private CalendarView.OnDateChangeListener dateChangeListener() {
        return new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                DateCalendar calendar = new DateCalendar(year, month, dayOfMonth);

                String date = calendar.getDateString();
                if (getSupportActionBar() != null) getSupportActionBar().setTitle(date);
                PeriodAdapter newAdapter = new PeriodAdapter(MainActivity.this, calendar);
                rv.swapAdapter(newAdapter, true);

                ScrollView cardSV = (ScrollView) findViewById(R.id.view_for_card);
                cardSV.removeAllViews();

                switch (calendar.compareCalendar()) {
                    case DateCalendar.SUNDAY:
                        detachItemTouchHelper(itemTouchHelper);
                        cardSV.addView(new CardGenerator(MainActivity.this).getHolidayCard());
                        break;
                    case DateCalendar.GREATER:
                        detachItemTouchHelper(itemTouchHelper);
                        break;
                    case DateCalendar.EQUAL:
                        initSwipe(newAdapter, false);
                        break;
                    case DateCalendar.SMALLER:
                        detachItemTouchHelper(itemTouchHelper);
                        DayRecord stats = dbHelper.getStat(calendar);
                        cardSV.addView(new CardGenerator(MainActivity.this).getStatView(stats, false, false));
                        break;
                }

            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_arrow:
                if (calendarView.getVisibility() == View.GONE) {
                    calendarView.setVisibility(View.VISIBLE);
                    item.setIcon(R.drawable.ic_expand_less_black_24dp);
                } else {
                    calendarView.setVisibility(View.GONE);
                    item.setIcon(R.drawable.ic_expand_more_black_24dp);
                }
                break;
            case R.id.menu_settings:
                Intent intent = new Intent(this, StatActivity.class);
                startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

}

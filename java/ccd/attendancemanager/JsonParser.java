package ccd.attendancemanager;

import android.content.Context;
import android.content.res.Resources;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

class JsonParser {

    private Context context;

    public JsonParser(Context context) {
        this.context = context;
    }

    private String getStringFromJson(int id) throws IOException {

        Resources res = context.getResources();
        InputStream ins = res.openRawResource(id);

        ByteArrayOutputStream result = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];
        int length;

        while ((length = ins.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }

        return result.toString();
    }

    public List<String> getSubjectsFromJson() {

        HashSet<String> subjectSet = new HashSet<>();

        try {
            String jsonStr = getStringFromJson(R.raw.time_table);
            JSONObject mainJson = new JSONObject(jsonStr);
            Iterator<String> daysItr = mainJson.keys();
            while (daysItr.hasNext()) {
                String weekDay = daysItr.next();
                JSONArray weekArray = mainJson.getJSONArray(weekDay);
                for (int i = 0; i < weekArray.length(); i++) {
                    String sub = weekArray.getJSONObject(i).getString("subject");
                    subjectSet.add(sub);
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return new ArrayList<>(subjectSet);
    }

    public List<String> getSubjectsWithRepetitionFromJson() {

        HashSet<String> subjectSet = new HashSet<>();

        try {
            String jsonStr = getStringFromJson(R.raw.time_table);
            JSONObject mainJson = new JSONObject(jsonStr);
            Iterator<String> daysItr = mainJson.keys();
            while (daysItr.hasNext()) {
                String weekDay = daysItr.next();
                JSONArray weekArray = mainJson.getJSONArray(weekDay);
                ArrayList<String> tmpList = new ArrayList<>();
                for (int i = 0; i < weekArray.length(); i++) {
                    String sub = weekArray.getJSONObject(i).getString("subject");
                    if (tmpList.contains(sub)) {
                        tmpList.add(sub);
                        sub = (Collections.frequency(tmpList, sub) - 1) + sub;
                    } else {
                        tmpList.add(sub);
                    }
                    subjectSet.add(sub);
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return new ArrayList<>(subjectSet);
    }

    public List<Period> parseJson(String weekDay) {
        List<Period> periodList = new ArrayList<>();

        if (weekDay.equalsIgnoreCase("Sunday")) return periodList;

        try {
            String jsonStr = getStringFromJson(R.raw.time_table);
            JSONObject mainJson = new JSONObject(jsonStr);
            JSONArray weekArray = mainJson.getJSONArray(weekDay);

            for (int i = 0; i < weekArray.length(); i++) {
                String subject = weekArray.getJSONObject(i).getString("subject");
                int startTimeHr = weekArray.getJSONObject(i).getInt("start_time_hour");
                int startTimeMin = weekArray.getJSONObject(i).getInt("start_time_min");
                int duration = weekArray.getJSONObject(i).getInt("duration");
                String teacher = weekArray.getJSONObject(i).getString("teacher");
                String classroom = weekArray.getJSONObject(i).getString("classroom");

                periodList.add(new Period(subject, startTimeHr,
                        startTimeMin, duration, teacher, classroom));
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }

        return periodList;
    }
}

package ccd.attendancemanager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

class Period {
    private String subject;
    private String teacher;
    private String classroom;

    private String startTime;
    private String endTime;

    private String tag;

    public static void generateTags(List<Period> periodList) {
        List<String> tmpList = new ArrayList<>();

        for (Period period : periodList) {
            String tag = period.getSubject().replaceAll("-", "_");

            if (tmpList.contains(tag)) {
                tmpList.add(tag);
                tag = (Collections.frequency(tmpList, tag) - 1) + tag;
            } else {
                tmpList.add(tag);
            }

            period.setTag(tag);

        }

    }

    public Period(String subject, int startTimeHr, int startTimeMin,
                  int duration, String teacher, String classroom) {
        this.subject = subject;
        this.teacher = teacher;
        this.classroom = classroom;

        this.startTime = computeStartTime(startTimeHr, startTimeMin);
        this.endTime = computeEndTime(startTimeHr, startTimeMin, duration);

        this.tag = subject;
    }

    private String computeStartTime(int hr, int min) {
        return computeTime(hr, min);
    }

    private String computeEndTime(int hr, int min, int duration) {
        while (duration > 60) {
            duration -= 60;
            hr++;
        }
        min += duration;

        return computeTime(hr, min);
    }

    private String computeTime(int hr, int min) {
        String time = String.valueOf(hr) + ":" + String.valueOf(min);
        try {
            Date date = new SimpleDateFormat("HH:mm", Locale.US).parse(time);
            time = new SimpleDateFormat("h:mm a", Locale.US).format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

    public String getSubject() {
        return subject;
    }

    public String getTeacher() {
        return teacher;
    }

    public String getClassroom() {
        return classroom;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    private void setTag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}

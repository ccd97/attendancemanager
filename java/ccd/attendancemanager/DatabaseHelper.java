package ccd.attendancemanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v4.util.Pair;

import java.util.HashMap;
import java.util.List;


class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "attendance";

    private static final String KEY_ID = "id";

    private List<String> subjectList;

    public DatabaseHelper(Context context, List<String> sub) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.subjectList = sub;
    }

    private String generateCreateQuery() {
        String result = "";
        for (String subject : subjectList) {
            subject = subject.replaceAll("-", "_");
            result += (subject + "_total");
            result += " INTEGER DEFAULT 0,";
            result += (subject + "_attended");
            result += " INTEGER DEFAULT 0,";
        }
        return result.substring(0, result.length() - 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + DATABASE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + generateCreateQuery() + ")";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);
            onCreate(db);
        }
    }

    /*
    protected static boolean doesDatabaseExist(Context context) {
        File dbFile = context.getDatabasePath(DATABASE_NAME);
        return dbFile.exists();
    }
    */

    private String[] getKeys() {
        String[] keys = new String[2 * subjectList.size() + 1];
        keys[0] = KEY_ID;
        int i = 1;
        for (String subject : subjectList) {
            subject = subject.replaceAll("-", "_");
            keys[i] = subject + "_total";
            keys[i + 1] = subject + "_attended";
            i += 2;
        }
        return keys;
    }

    private DayRecord getStat(int id) {
        return getStat(new DayRecord(id));
    }

    public DayRecord getStat(DateCalendar calendar) {
        return getStat(new DayRecord(calendar));
    }

    private DayRecord getStat(DayRecord record) {

        SQLiteDatabase database = this.getReadableDatabase();

        Cursor cursor = database.query(DATABASE_NAME, getKeys(), KEY_ID + "=?",
                new String[]{String.valueOf(record.getId())}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        for (String subject : subjectList) {
            subject = subject.replaceAll("-", "_");
            assert cursor != null;
            int total, attended;
            try {
                total = cursor.getInt(cursor.getColumnIndex(subject + "_total"));
                attended = cursor.getInt(cursor.getColumnIndex(subject + "_attended"));
            } catch (Exception ignored) {
                total = 0;
                attended = 0;
            }

            record.addField(subject, attended, total);
        }

        if (cursor != null)
            cursor.close();

        database.close();

        return record;
    }

    private int getSumOfColumn(String columnName) {
        int sum = 0;

        SQLiteDatabase database = this.getReadableDatabase();

        Cursor cursor = database.rawQuery
                ("SELECT SUM(" + columnName + ") FROM " + DATABASE_NAME, null);

        if (cursor.moveToFirst())
            sum = cursor.getInt(0);

        cursor.close();
        database.close();

        return sum;
    }

    public DayRecord getTotalRecord() {
        DayRecord record = new DayRecord();

        for (String subject : subjectList) {
            subject = subject.replaceAll("-", "_");
            int total, attended;
            try {
                total = getSumOfColumn(subject + "_total");
                attended = getSumOfColumn(subject + "_attended");
            } catch (Exception ignored) {
                total = 0;
                attended = 0;
            }

            record.addField(subject, attended, total);
        }

        return record;
    }

    public void addStat(DayRecord record) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        HashMap<String, Pair<Integer, Integer>> records = record.getRecord();

        for (String key : records.keySet()) {
            Pair<Integer, Integer> pair = records.get(key);
            int attended = pair.first;
            int total = pair.second;

            key = key.replaceAll("-", "_");
            values.put(key + "_total", total);
            values.put(key + "_attended", attended);
        }

        values.put(KEY_ID, record.getId());
        database.insertWithOnConflict(DATABASE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);

        database.close();
    }

    public void updateRecord(DayRecord record) {
        updateRecord(record, -1);
    }

    private void updateRecord(DayRecord record, int id) {
        DayRecord totalRecord = getTotalRecord();
        DayRecord prevRecord = getStat(id);
        record = record.subtract(totalRecord).add(prevRecord);

        addStat(record);
    }
}
